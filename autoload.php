<?php
// fonction permetant de charger les classes automatiquement
function shopAutoload( $classe ) {

    $folders = [
        'app/classes',
        'app/classes/models',
        'app/controllers'
    ];

    foreach ($folders as $folder) {
        
        $file = $folder . '/' . $classe . '.php';

        if( file_exists( $file ) ) {
            require_once $file;
            return true;
        }

    }

}
// enregistrement de la fonction permettant l'autoload
spl_autoload_register( 'shopAutoload' );
