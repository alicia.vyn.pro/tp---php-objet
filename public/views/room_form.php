<!-- l'utilisateur n'aura accès à cette page que s'il est connecté et a le rôle Annonceur -->
<?php if(Auth::isLogged() && Auth::user()->hasRole(Role::Annonceur) ):?>
<div>
    
    <div>
        <h2>Ajouter une annonce</h2>

        <div class="alert alert-info" role="alert">
            Tout les champs sont obligatoires
        </div>

        <?php include 'partials/errors.php' ?>

        <form action="/new/room" method="POST">
            <!-- Titre annonce -->
            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <label>Titre de votre annonce</label>
                    <input type="text" name="title" class="form-control">
                </div>
                </div>
            </div>

            <!-- Taille / Prix / Couchage -->
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label>Taille</label>
                    <div class="input-group">
                        <input type="number" step="0.5" name="size" class="form-control">
                        <div class="input-group-prepend">
                            <span class="input-group-text">m²</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label>Prix pour la nuit</label>
                    <div class="input-group">
                        <input type="number" step="0.01" name="price" class="form-control">
                        <div class="input-group-prepend">
                            <span class="input-group-text">€</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label>Nombre de couchages</label>
                    <input type="number" step="1" name="bedding" class="form-control">
                </div>
                </div>
            </div>

            <!-- Num rue / Adresse complète -->
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label>Numéro</label>
                    <input type="text" name="numero" class="form-control">
                </div>
                <div class="col-md-9 mb-3">
                    <label>Adresse complète</label>
                    <input type="text" name="rue" class="form-control">
                </div>
            </div>

            <!-- CP / Ville / Pays -->
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label>Code Postal</label>
                    <input type="number" step="1" name="cp" class="form-control">
                </div>
                <div class="col-md-6 mb-3">
                    <label>Ville</label>
                    <input type="text" name="ville" class="form-control">
                </div>
                <div class="col-md-3 mb-3">
                    <label>Pays</label>
                    <input type="text" name="pays" class="form-control">
                </div>
            </div>

            <!-- Description -->
            <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control" rows="5"></textarea>
            </div>
            
            <!-- Type logement -->
            <div class="form-group">
                <label>Type de location</label>
                <select name="type_id" class="custom-select">
                    <option selected>Selectionner un type de location</option>
                    <?php foreach ($types as $type ): ?>
                        <option value="<?php echo $type->getId() ?>"><?php echo $type->getLabel() ?></option>
                    <?php endforeach ?>
                </select>
            </div>

            <!-- Equipements -->
            <div class="form-group">
                <label>Equipements</label>
                <br>
                <?php foreach ($equipments as $equipment ): ?>
                <div class="form-check form-check-inline">
                    <input name="equipments[]" class="form-check-input" type="checkbox" value="<?php echo $type->getId() ?>">
                    <label class="form-check-label" ><?php echo $equipment->getLabel() ?></label>
                </div>
                <?php endforeach ?>
            </div>

            <button class="btn btn-primary" type="submit">Envoyer</button>
        </form>

    </div>

</div>
<?php endif ?>