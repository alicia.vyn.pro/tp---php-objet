<?php include 'partials/errors.php' ?>

<div class="row">
    <div class="col-md-6">
    
        <div>
            <h2>Login</h2>

            <form action="/login" method="POST">

                <label>
                    <span>Username</span>
                    <input type="text" name="username" class="form-control">
                </label>

                <label>
                    <span>Password</span>
                    <input type="password" name="password" class="form-control">
                </label>

                <label>
                    <input type="submit" value="Login" class="btn btn-primary">
                </label>
                
                
            </form>

        </div>
    
    </div>
    <div class="col-md-6">
    
        <div>
            <h2>Signin</h2>

            <form action="/signin" method="POST">

                <label>
                    <span>Username</span>
                    <input type="text" name="username" class="form-control">
                </label>

                <label>
                    <span>Password</span>
                    <input type="password" name="password" class="form-control">
                </label>

                <label>
                    <span>Confirm Password</span>
                    <input type="password" name="password_check" class="form-control">
                </label>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="role" value="1">
                    <label class="form-check-label">Annonceur</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="role" value="2">
                    <label class="form-check-label">Utilisateur</label>
                </div>


                <label>
                    <input type="submit" value="Signin" class="btn btn-primary">
                </label>

            </form>

        </div>
    
    </div>

</div>

