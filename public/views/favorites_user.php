<!-- l'utilisateur n'aura accès à cette page que s'il est connecté et a le rôle Utilisateur -->
<?php if(Auth::isLogged() && Auth::user()->hasRole(Role::Utilisateur) ):?>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Location</th>
            <th scope="col">Taille en m²</th>
            <th scope="col">Prix / nuit</th>
            <th scope="col">Couchages</th>
            <th scope="col">Voir la location</th>
            <th scope="col">Retirer des favoris</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($favlists as $favlist): ?>
            <tr>
                <td><?php echo $favlist->getTitle() ?></td>
                <td><?php echo $favlist->getSize() ?></td>
                <td><?php echo $favlist->getPrice() ?></td>
                <td><?php echo $favlist->getBedding() ?></td>
                <td>
                    <a href="/room/<?php echo $favlist->getId() ?>">Détails</a>
                </td>
                <td>
                    <form action="/favorites/remove/<?php echo $favlist->getId() ?>" method="POST">
                        <input type="submit" class="btn btn-outline-info" value="Retirer des favoris">
                    </form>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
<?php endif ?>