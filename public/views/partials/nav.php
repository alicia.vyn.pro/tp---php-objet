<nav>
    <ul class="nav">
        <li class="nav-item"><a href="/" class="nav-link">
            <i class="fas fa-home"></i> Accueil</a>
        </li>
        <?php if( empty( $_SESSION['user'] ) ): ?>
        <li class="nav-item"><a href="/authentication" class="nav-link">
            <i class="fas fa-sign-in-alt"></i> Login / Signin</a>
        </li>
        <?php endif ?>

        <?php if( Auth::isLogged() ): ?>
        <li class="nav-item"><a href="/disconnect" class="nav-link">
            <i class="fas fa-sign-in-alt"></i> Logout</a>
        </li>
        <?php endif ?>
    </ul>

    <ul class="nav">
        <?php if(Auth::isLogged() && Auth::user()->hasRole(Role::Annonceur) ):?>
        <li class="nav-item"><a href="/add/room" class="nav-link">
            <i class="fas fa-plus-square"></i> Ajouter une annonce</a>
        </li>

        <li class="nav-item"><a href="/my_rooms" class="nav-link">
            <i class="fas fa-clipboard-list"></i> Voir mes annonces</a>
        </li>

        <li class="nav-item"><a href="/announcer/reservations" class="nav-link">
            <i class="fas fa-calendar-alt"></i> Voir les réservations</a>
        </li>
        <?php endif ?>

        <?php if(Auth::isLogged() && Auth::user()->hasRole(Role::Utilisateur) ):?>
        <li class="nav-item"><a href="/user/reservations" class="nav-link">
            <i class="fas fa-calendar-alt"></i> Voir mes réservations</a>
        </li>
        <li class="nav-item"><a href="/user/favorites" class="nav-link">
                <i class="fas fa-star"></i> Voir mes favoris</a>
        </li>
        <?php endif ?>

    </ul>
</nav>
