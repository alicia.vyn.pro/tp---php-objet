<?php if( !empty($errors) ): ?>

    <div class="alert alert-warning" role="alert">

        <?php foreach ($errors as $error ): ?>

            <div><?php echo $error ?></div>

        <?php endforeach ?>

    </div>

<?php endif ?>