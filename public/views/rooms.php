<?php include 'partials/errors.php' ?>

<div class="card-columns">

<?php foreach ($rooms as $room): ?>
    
    <div class="card">
        <div class="card-body">
            <h2 class="card-title"><?php echo $room->getTitle() ?></h2>
            <hr>
            <h3 class="card-title"><?php echo $room->getSize() ?>m²</h3>
            <h3 class="card-title"><?php echo $room->getPrice() ?>€ la nuit</h3>
            <p class="card-text"><?php echo $room->getBedding() ?> couchages</p>
            <p class="card-text text-justify"><?php echo $room->getDescription() ?></p>
            <a href="/room/<?php echo $room->getId() ?>" class="btn btn-primary mb-md-3">Voir la location</a>

            <!-- l'utilisateur n'aura accès à ce bouton que s'il est connecté et a le rôle Utilisateur -->
			<?php if( Auth::isLogged() && Auth::user()->hasRole(Role::Utilisateur) ): ?>
                <form action="/favorites/add/<?php echo $room->getId() ?>" method="POST">
                    <input type="submit" class="btn btn-outline-warning" value="Ajouter aux favoris">
                </form>
			<?php endif ?>
        </div>
    </div>
    
<?php endforeach ?>

</div>
