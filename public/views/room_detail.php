<?php include 'partials/errors.php' ?>

<div>
    <h2><?php echo $room->getTitle() ?></h2>
    <span>Proposé par <?php echo $user->getUsername() ?></span>
    <hr>
    <span class="h3"><?php echo $type->getLabel() ?></span>
    <br>
    <span class="h4"><?php echo $room->getSize() ?>m²</span> -
    <span class="h4"><?php echo $room->getPrice() ?>€ la nuit</span>
    <p><?php echo $room->getDescription() ?></p>
    <p><?php echo $room->getBedding() ?> couchages</p>

    <?php foreach ($equips as $equip): ?>
        <span><?php echo $equip->getLabel() ?></span>
    <?php endforeach ?>

    <p>Situé au 
        <?php echo $adress->getNumero() ?> 
        <?php echo $adress->getRue() ?> - 
        <?php echo $adress->getCp() ?> 
        <?php echo $adress->getVille() ?>, 
        <?php echo $adress->getPays() ?>
    </p>

    <!-- l'utilisateur n'aura accès à ce bouton que s'il est connecté et a le rôle Utilisateur -->
	<?php if( Auth::isLogged() && Auth::user()->hasRole(Role::Utilisateur) ): ?>
        <form action="/favorites/add/<?php echo $room->getId() ?>" method="POST">
            <input type="submit" class="btn btn-outline-warning" value="Ajouter aux favoris">
        </form>
	<?php endif ?>

    <hr>

    <form action="/reservation/<?php echo $room->getId() ?>" method="POST">
        <div class="text-center">
            <div class="h3">Reservation</div>
            du 
            <input type="date"  
                    name="date_start" 
                    value="<?php echo date('Y-m-d') ?>">
            au 
            <input type="date" 
                    name="date_end"
                    value="<?php echo date('Y-m-d', strtotime( '+1 week' )) ?>">

            <br><br>
            <button class="btn btn-primary" type="submit">Réserver cette location</button>
        </div>
    </form>
    
</div>