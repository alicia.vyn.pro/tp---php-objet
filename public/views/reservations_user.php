<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Date de début</th>
      <th scope="col">Date de fin</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($reservations as $reservation): ?>
        <tr>
          <td><?php echo $reservation->getDate_start() ?></td>
          <td><?php echo $reservation->getDate_end() ?></td>
        </tr>
    <?php endforeach ?>

  </tbody>
</table>