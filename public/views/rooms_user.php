<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Location</th>
      <th scope="col">Taille en m²</th>
      <th scope="col">Prix / nuit</th>
      <th scope="col">Couchages</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($rooms as $room): ?>
        <tr>
        <td><?php echo $room->getTitle() ?></td>
        <td><?php echo $room->getSize() ?></td>
        <td><?php echo $room->getPrice() ?></td>
        <td><?php echo $room->getBedding() ?></td>
        </tr>
    <?php endforeach ?>
  </tbody>
</table>