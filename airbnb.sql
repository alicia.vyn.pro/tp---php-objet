-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour airbnb---tp-php-objet
CREATE DATABASE IF NOT EXISTS `airbnb---tp-php-objet` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `airbnb---tp-php-objet`;

-- Export de la structure de la table airbnb---tp-php-objet. adress
CREATE TABLE IF NOT EXISTS `adress` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `numero` int(10) unsigned DEFAULT NULL,
  `rue` varchar(50) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `ville` varchar(50) NOT NULL,
  `pays` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Export de données de la table airbnb---tp-php-objet.adress : ~6 rows (environ)
/*!40000 ALTER TABLE `adress` DISABLE KEYS */;
REPLACE INTO `adress` (`id`, `numero`, `rue`, `cp`, `ville`, `pays`) VALUES
	(1, 2026, 'Terra Street', 98119, 'Seattle', 'United States'),
	(2, 1, 'rue de Paris', 31452, 'Toulouse', 'France'),
	(3, 818, 'Victoria Street', 60631, 'Chicago', 'United States'),
	(7, 13, 'rue Beauvau', 13001, 'Marseille', 'France'),
	(8, 1, 'cours Lazare Escarguel', 66000, 'Perpignan', 'France'),
	(9, 19, 'rue des Poissons-chats', 6000, 'Nice', 'France');
/*!40000 ALTER TABLE `adress` ENABLE KEYS */;

-- Export de la structure de la table airbnb---tp-php-objet. equipments
CREATE TABLE IF NOT EXISTS `equipments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Export de données de la table airbnb---tp-php-objet.equipments : ~4 rows (environ)
/*!40000 ALTER TABLE `equipments` DISABLE KEYS */;
REPLACE INTO `equipments` (`id`, `label`) VALUES
	(1, 'grille-pain'),
	(2, 'machine à laver'),
	(3, 'frigo'),
	(4, 'télévision');
/*!40000 ALTER TABLE `equipments` ENABLE KEYS */;

-- Export de la structure de la table airbnb---tp-php-objet. favorites
CREATE TABLE IF NOT EXISTS `favorites` (
  `user_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  KEY `user_id_room_id` (`user_id`,`room_id`),
  KEY `FK_favorites_rooms` (`room_id`),
  CONSTRAINT `FK_favorites_rooms` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `FK_favorites_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table airbnb---tp-php-objet.favorites : ~3 rows (environ)
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
REPLACE INTO `favorites` (`user_id`, `room_id`) VALUES
	(2, 7),
	(2, 8);
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;

-- Export de la structure de la table airbnb---tp-php-objet. reservations
CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_room_id_date_start_date_end` (`user_id`,`room_id`,`date_start`,`date_end`),
  KEY `user_id` (`user_id`),
  KEY `FK_reservations_rooms` (`room_id`),
  CONSTRAINT `FK_reservations_rooms` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_reservations_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Export de données de la table airbnb---tp-php-objet.reservations : ~3 rows (environ)
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
REPLACE INTO `reservations` (`id`, `user_id`, `room_id`, `date_start`, `date_end`) VALUES
	(1, 2, 1, '2019-02-01', '2019-02-15'),
	(2, 2, 8, '2019-05-27', '2019-06-27'),
	(3, 4, 8, '2019-01-30', '2019-02-06');
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;

-- Export de la structure de la table airbnb---tp-php-objet. roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Export de données de la table airbnb---tp-php-objet.roles : ~2 rows (environ)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
REPLACE INTO `roles` (`id`, `label`) VALUES
	(1, 'Annonceur'),
	(2, 'Utilisateur');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Export de la structure de la table airbnb---tp-php-objet. rooms
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `size` float unsigned NOT NULL,
  `price` float unsigned NOT NULL,
  `bedding` int(10) unsigned NOT NULL,
  `description` text,
  `type_id` int(10) unsigned NOT NULL,
  `adress_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rooms_types` (`type_id`),
  KEY `FK_rooms_users` (`user_id`),
  KEY `FK_rooms_adress` (`adress_id`),
  CONSTRAINT `FK_rooms_adress` FOREIGN KEY (`adress_id`) REFERENCES `adress` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_rooms_types` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`),
  CONSTRAINT `FK_rooms_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Export de données de la table airbnb---tp-php-objet.rooms : ~6 rows (environ)
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
REPLACE INTO `rooms` (`id`, `title`, `size`, `price`, `bedding`, `description`, `type_id`, `adress_id`, `user_id`) VALUES
	(1, 'Louez-moi', 45, 90, 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et fringilla diam, rhoncus aliquet lacus. ', 1, 1, 3),
	(2, 'Logement', 56, 105, 3, 'blablabla', 1, 3, 1),
	(6, 'Hello World', 45, 45, 45, 'test', 3, 7, 3),
	(7, 'Test', 51, 51, 3, 'Ma jolie maison', 2, 8, 3),
	(8, 'T4 avec vue sur la mer', 85, 150, 6, 'test', 1, 9, 1);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;

-- Export de la structure de la table airbnb---tp-php-objet. room_equip
CREATE TABLE IF NOT EXISTS `room_equip` (
  `room_id` int(10) unsigned NOT NULL,
  `equip_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `room_id_equip_id` (`room_id`,`equip_id`),
  KEY `FK_room_equip_equipements` (`equip_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `FK_room_equip_equipements` FOREIGN KEY (`equip_id`) REFERENCES `equipments` (`id`),
  CONSTRAINT `FK_room_equip_rooms` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table airbnb---tp-php-objet.room_equip : ~4 rows (environ)
/*!40000 ALTER TABLE `room_equip` DISABLE KEYS */;
REPLACE INTO `room_equip` (`room_id`, `equip_id`) VALUES
	(1, 3),
	(7, 3),
	(8, 3),
	(1, 4);
/*!40000 ALTER TABLE `room_equip` ENABLE KEYS */;

-- Export de la structure de la table airbnb---tp-php-objet. types
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Export de données de la table airbnb---tp-php-objet.types : ~2 rows (environ)
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
REPLACE INTO `types` (`id`, `label`) VALUES
	(1, 'Logement entier'),
	(2, 'Chambre privée'),
	(3, 'Chambre partagée');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;

-- Export de la structure de la table airbnb---tp-php-objet. users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_users_roles` (`role_id`),
  CONSTRAINT `FK_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Export de données de la table airbnb---tp-php-objet.users : ~2 rows (environ)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `username`, `password`, `role_id`) VALUES
	(1, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1),
	(2, 'alicia', '4d1e58c90b3b94bcad9848eccacd6d2a8c9fbc5ca913304bba5cdeab36feefa3', 2),
	(3, 'test', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 1),
	(4, 'a', 'ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb', 2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
