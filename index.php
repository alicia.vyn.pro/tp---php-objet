<?php
require_once 'app/flight/Flight.php';
require_once 'autoload.php';
require_once 'CONFIG.php';

session_start();

$authCT = new AuthController;
Flight::route( '/authentication',   [ $authCT, 'form' ] );
Flight::route( 'POST /login',       [ $authCT, 'login' ] );
Flight::route( 'POST /signin',      [ $authCT, 'signin' ] );
Flight::route( '/disconnect',       [ $authCT, 'logout' ]);


$roomCT = new RoomController;
Flight::route( '/',                 [ $roomCT, 'index' ] );
Flight::route( '/room/@id:[0-9]*',  [ $roomCT, 'detail' ] );
Flight::route( '/add/room',         [ $roomCT, 'form' ] );
Flight::route( 'POST /new/room',    [ $roomCT, 'newroom' ] );
Flight::route( '/my_rooms',         [ $roomCT, 'myrooms' ] );

$reservationCT = new ReservationController;
Flight::route( 'POST /reservation/@id:[0-9]*', [ $reservationCT, 'newreservation' ] );
Flight::route( '/user/reservations',           [ $reservationCT, 'userreservations' ] );
Flight::route( '/announcer/reservations',      [ $reservationCT, 'announcerreservations' ] );

$favoriteCT = new FavoriteController;
Flight::route( 'POST /favorites/add/@id:[0-9]*', 		[ $favoriteCT, 'newfavorite' ] );
Flight::route( 'POST /favorites/remove/@id:[0-9]*', 	[ $favoriteCT, 'removefavorite' ] );
Flight::route( '/user/favorites', 					    [ $favoriteCT, 'userfavorites' ] );

Flight::start();
