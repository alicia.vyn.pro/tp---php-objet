<?php

abstract class CheckAnnouncerController extends CheckController {

    // vérifie si l'utilisateur à les autorisations nécessaire
    protected function canAccess(): bool {
        return Auth::isLogged() && Auth::user()->hasRole( Role::Annonceur );
    }

    // rendirige et affiche une erreur en cas de mauvais role
    protected function errorAction() {

        $this->addError('Vous n\'êtes pas autorisé à acceder a cette page');
        $this->redirect('/authentication');
    }
    
}
