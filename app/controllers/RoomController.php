<?php

class RoomController extends CheckAnnouncerController {

    // affiche toute les chambres sur l'index
    function index() {

        $rooms = Room::findAll();

        $this->render('rooms', compact( 'rooms' ));
    }

    // trouve et affiche le détail de chaque chambre
    function detail( int $id ) {

        $room = new Room;

        if($room->find( $id )) {

            $user = new User;
            $user_id = $room->getUser_id();
            $user->find( $user_id );

            $type = new Type;
            $type_id = $room->getType_id();
            $type->find( $type_id );

            $adress = new Adress;
            $adress_id = $room->getAdress_id();
            $adress->find( $adress_id );

            $equip = new Equipment;
            $equips = $equip->findByRoomId( $id );

            $this->render( 'room_detail', compact( 'room', 'user', 'type', 'adress', 'equips' ) );
        
        }
        else $this->notFound();

    }

    // affiche le forumlaire d'ajout de chambre
    function form() {
        $this->checkAccess();

        $types = Type::findAll();
        $equipments = Equipment::findAll();

        $this->render('room_form', compact( 'types', 'equipments' ));
    }

    // ajout d'une chambre
    function newroom() {
        $this->checkAccess();

        $adress = new Adress;
        $room = new Room;

        // vérification des champs
         if( $this->checkFields( [
                'title', 'size', 'price', 'bedding',
                'numero', 'rue', 'cp', 'ville', 'pays',
                'description', 'type_id', 'equipments'
            ] ) ) {

            $adress = new Adress( 
                $this->fields
             );
            $room = new Room( 
                $this->fields
            );

            // création en BDD
            if( $adress->create() ) {
                    $room->setAdress_id( $adress->getId());
                    $room->setUser_id( $_SESSION['user']->getId() );
                    $room->create();
                    
                    // on récupère les équipements depuis le forumlaire
                    foreach($this->fields['equipments'] as $equipment_id) {
                        $room->addEquipmentById( $equipment_id );
                    }
    
                    // on insère les équipements en BDD en les liant à la chambre
                    $room->linkEquipments();
        
                    $this->redirect('/');
            }
            else $this->addError('Une erreur est survenue');
        }
        else $this->addError('Il manque des champs');
        $this->redirect('/add/room');

    }

    // affichage des chambres liées à l'annonceur
    function myrooms() {
        
        $user_id = $_SESSION['user']->getId();
        $rooms = Room::findAllByUserId( $user_id );

        $this->render('rooms_user', compact( 'rooms' ));
    }

}