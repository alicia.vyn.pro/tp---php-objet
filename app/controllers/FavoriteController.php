<?php

class FavoriteController extends Controller {

	// affichage des favoris
	function userfavorites() {
		$favorites = new Favorite( Auth::user() );
		$favlists = $favorites->getFavorites();

		$this->render('favorites_user', compact( 'favlists' ) );
	}

	// ajout d'un favoris
	function newfavorite( int $id ) {

		$favorites = new Favorite( Auth::user() );

		// vérifie si la chambre est déjà dans les favoris
		if( $favorites->hasRoom( $id ) ) {
			$this->addError('Cette location est déjà dans vos favoris');
		}
		else {
			// ajoute le favoris
			if( $favorites->saveRoom( $id ) ) {
				$this->redirect('/user/favorites');
			}
			else $this->addError('Une erreur est survenue');
		}

		$this->redirect('/');

	}

	// supression d'un favoris
	function removefavorite( int $id ) {

		$favorites = new Favorite( Auth::user() );

		if( $favorites->deleteRoom( $id ) ) {
			$this->redirect('/user/favorites');
		}

	}

}