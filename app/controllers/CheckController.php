<?php

abstract class CheckController extends Controller {

abstract protected function canAccess(): bool;
abstract protected function errorAction();

// renvoi une erreur si l'utilisateur n'a pas le role nécessaire
protected function checkAccess() {

    if( !$this->canAccess() ) $this->errorAction();

}

}
