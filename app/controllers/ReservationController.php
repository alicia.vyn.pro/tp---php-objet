<?php

class ReservationController extends Controller {

    // ajoute une réservation
    function newreservation( int $id ) {

        // vérifie les champs du formulaire
        if( $this->checkFields([ 'date_start', 'date_end' ]) ) {

            $reservation = new Reservation( $this->fields );

            $reservation->setUser_id( $_SESSION['user']->getId() );
            $reservation->setRoom_id( $id );

            // créé la réservation
            if( $reservation->create( $id ) ) {
                $this->redirect('/');
            }
            else $this->addError('Une erreur est survenue');

        }
        else $this->addError('Il manque des champs');
        $this->redirect('/room/' . $id);


    }

    // affichage des réservations côté Utilisateur
    function userreservations() {

        $user_id = $_SESSION['user']->getId();
        $reservations = Reservation::findAllByUserId( $user_id );

        $this->render('reservations_user', compact( 'reservations' ));
        
    }

    // affichage des réservations côté Annonceur
    function announcerreservations() {
        $user_id = $_SESSION['user']->getId();
        $reservations =  Reservation::findReservations( $user_id );
 
        $this->render( 'reservations_announcer', compact( 'reservations' ) );

    }

}