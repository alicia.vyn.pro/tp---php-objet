<?php

class AuthController extends Controller {

    function form() {
        $this->render('authentication');
    }


    function login() {

        if( $this->checkFields(['username', 'password']) ) {

            $auth = new Auth( $this->fields['username'], $this->fields['password'] );

            if( $user = $auth->login() )
                $this->redirect('/');
            else 
                $this->addError('Utilisateur ou mot de passe incorrect !');

        }
        else $this->addError('Il manque des champs');

        $this->redirect('/authentication');

    }

    function signin() {

        if( $this->checkFields(['username', 'password', 'password_check', 'role']) ) {

            $auth = new Auth( 
                $this->fields['username'], 
                $this->fields['password'], 
                $this->fields['password_check'],
                $this->fields['role']
            );


            if( $auth->userExist() )
            $this->addError('L\'utilisateur existe déjà');

            else if( !$auth->checkPasswords() )
                $this->addError('Les mots de passe ne correspondent pas');

            else if( $user = $auth->signin() )
                $this->redirect('/');

            else 
                $this->addError('Une erreur est survenue');

            }
        else $this->addError('Il manque des champs');

        $this->redirect('/authentication');

    }

    function logout(){
        unset( $_SESSION['user'] );
        $this->redirect('/authentication');
    }
}