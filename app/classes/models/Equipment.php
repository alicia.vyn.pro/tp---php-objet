<?php

class Equipment extends Model {

	private $label;

	#region Getters & Setters
	public function getLabel()
	{
		return $this->label;
	}

	public function setLabel($label)
	{
		$this->label = $label;

		return $this;
	}
	#endregion

	#region Define abstracts
	protected function getTable(): string
	{
		return 'equipments';
	}

	#endregion

	// recherche dans la BDD via l'Id de la chambre
	public static function findByRoomId(int $room_id)
	{

		$sql = "SELECT equipments.* FROM equipments
                JOIN room_equip AS re
                ON re.equip_id = equipments.id
                WHERE re.room_id = :id";
		$stmt = (new Bdd)->getPdo()->prepare($sql);
		$stmt->execute([
			'id' => $room_id
		]);
		$results = $stmt->fetchAll();

		$models = [];

		foreach ($results as $result) {

			$model = new Equipment($result);
			$models[] = $model;

		}

		return $models;

	}


}

