<?php

class Type extends Model {

    private $label;

    #region Getters & Setters
    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }
    #endregion

    #region Define abstracts
    protected function getTable(): string {
        return 'types';
    }
    #endregion
}
