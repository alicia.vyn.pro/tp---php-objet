<?php

class Adress extends Model {

    private $numero;
    private $rue; 
    private $cp;
    private $ville;
    private $pays;

    #region Getters & Setters
    public function getNumero()
    {
        return $this->numero;
    }

    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }
 
    public function getRue()
    {
        return $this->rue;
    }
 
    public function setRue($rue)
    {
        $this->rue = $rue;

        return $this;
    }
 
    public function getCp()
    {
        return $this->cp;
    }
 
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    public function getVille()
    {
        return $this->ville;
    }

    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    public function getPays()
    {
        return $this->pays;
    }

    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }
    #endregion

    #region Define abstracts
    protected function getTable(): string {
        return 'adress';
    }
    #endregion

    // insertion dans la base de donnée
    public function create() {

        $sql = 'INSERT INTO adress VALUES (0, :numero, :rue, :cp, :ville, :pays)';
        $pdo = $this->bdd->getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            'numero'    => $this->numero,
            'rue'       => $this->rue,
            'cp'        => $this->cp,
            'ville'     => $this->ville,
            'pays'      => $this->pays
        ]);

        if( $stmt->rowCount() == 0 ) return false;

        $this->id = $pdo->lastInsertId();
        return true;

    }

}