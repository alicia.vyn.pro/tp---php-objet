<?php 
abstract class Model {

    protected $id;

    protected $bdd;

    #region Getters & Setters
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    #endregion

    abstract protected function getTable(): string;

    public function __construct( array $datas = [] ) {
        
        $this->bdd = new Bdd;
        $this->hydrate( $datas );

    }

    // function hydrate afin de génèrer dynamiquement les méthodes setters
    private function hydrate( array $datas ) {
        
        foreach( $datas as $key => $data ) {

            $setter = 'set' . ucfirst($key);

            if( method_exists( $this, $setter ) ) {

                $this->{$setter}( $data );

            }
            
        }

    }

    // recherche dans la BDD via l'id
    public function find( int $id ) {

        $this->id = $id;

        $sql = "SELECT * FROM {$this->getTable()} WHERE id = :id";
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute([ 
            'id' => $this->id 
            ]);
        $results = $stmt->fetch();
        
        if( !$results ) return false;

        $this->hydrate( $results );
        return true;

    }

    #region Method static
    // recherche de toute les entrées dans la BDD
    public static function findAll(): array {

        $classname = get_called_class(); 
        $class = new $classname;

        $sql = "SELECT * FROM {$class->getTable()}";
        $results = $class->bdd->getPdo()->query($sql)->fetchAll();

        $models = [];

        foreach ( $results as $result ) {
            
            $model = new $classname( $result );
            $models[] = $model;
            
        }

        return $models;

    }

    // recherche dans la BDD via l'id user
    public static function findAllByUserId( int $user_id ) {

        $classname = get_called_class(); 
        $class = new $classname;

        $sql = "SELECT * FROM {$class->getTable()} WHERE user_id = :user_id";
        $stmt = $class->bdd->getPdo()->prepare( $sql );
        $stmt->execute([
            'user_id' => $user_id
        ]);
        $results = $stmt->fetchAll();

        $models = [];

        foreach ( $results as $result ) {
            
            $model = new $classname( $result );
            $models[] = $model;
            
        }

        return $models;

    }

    #endregion

}
