<?php

class Reservation extends Model {

    private $user_id;
    private $room_id;
    private $date_start;
    private $date_end;
    
    #region Getters & Setters
    public function getUser_id()
    {
        return $this->user_id;
    }

    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }
 
    public function getRoom_id()
    {
        return $this->room_id;
    }

    public function setRoom_id($room_id)
    {
        $this->room_id = $room_id;

        return $this;
    }

    public function getDate_start()
    {
        return $this->date_start;
    }
    
    public function setDate_start($date_start)
    {
        $this->date_start = $date_start;

        return $this;
    }
 
    public function getDate_end()
    {
        return $this->date_end;
    }

    public function setDate_end($date_end)
    {
        $this->date_end = $date_end;

        return $this;
    }
    #endregion

    #region Define abstracts
    protected function getTable(): string {
        return 'reservations';
    }
    #endregion    

    // insertion dans la BDD
    public function create( $id ) {

        $this->id = $id;

        $sql = 'INSERT INTO reservations VALUES (0, :user_id, :room_id, :date_start , :date_end)';
        $pdo = $this->bdd->getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            'user_id'         => $this->user_id,
            'room_id'         => $this->id,
            'date_start'      => $this->date_start,
            'date_end'        => $this->date_end
        ]);

        if( $stmt->rowCount() == 0 ) return false;

        $this->id = $pdo->lastInsertId();
        return true;

    }

    // recherche de la réservation via l'user id dans la session
    public static function findReservations( int $user_id ) {

        $sql = 'SELECT reservations.*, rooms.* FROM reservations
                JOIN rooms AS rooms
                ON reservations.room_id = rooms.id
                WHERE rooms.user_id = :id';
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute([
            'id' => $user_id
        ]);
        $results = $stmt->fetchAll();

        $models = [];

        foreach ( $results as $result ) {
            
            $model = new Reservation( $result );
            $models[] = $model;
            
        }

        return $models;

    }

}