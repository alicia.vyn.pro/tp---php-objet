<?php

class Room extends Model {

	private $title;
	private $size;
	private $price;
	private $bedding;
	private $description;
	private $type_id;
	private $adress_id;
	private $user_id;
	private $equipments = [];

	#region Getters & Setters
	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;

		return $this;
	}

	public function getSize() {
		return $this->size;
	}

	public function setSize($size) {
		$this->size = $size;

		return $this;
	}

	public function getPrice() {
		return $this->price;
	}

	public function setPrice($price) {
		$this->price = $price;

		return $this;
	}

	public function getBedding() {
		return $this->bedding;
	}

	public function setBedding($bedding) {
		$this->bedding = $bedding;

		return $this;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription($description) {
		$this->description = $description;

		return $this;
	}

	public function getType_id() {
		return $this->type_id;
	}

	public function setType_id($type_id) {
		$this->type_id = $type_id;

		return $this;
	}

	public function getAdress_id() {
		return $this->adress_id;
	}

	public function setAdress_id($adress_id) {
		$this->adress_id = $adress_id;

		return $this;
	}

	public function getUser_id() {
		return $this->user_id;
	}

	public function setUser_id($user_id) {
		$this->user_id = $user_id;

		return $this;
	}
	#endregion

	#region Define abstracts
	protected function getTable(): string {
		return 'rooms';
	}

	#endregion

	// 
	public function addEquipmentById(int $id) {

		$equipment = new Equipment;
		if ($equipment->find($id)) {
			$this->equipments[] = $equipment;
		}

	}

	// permet de lier les équipements à la chambre correspondante dans la Bdd
	public function linkEquipments(): bool {

		$sql = 'INSERT INTO room_equip VALUES ';
		foreach ($this->equipments as $equipment) {
			$sql .= "( {$this->id}, {$equipment->getId()} ), ";
		}
		$sql = rtrim($sql, ', ');

		$stmt = $this->bdd->getPdo()->query($sql);

		return $stmt->rowCount() > 0;

	}

	// insertion en BDD
	public function create() {

		$sql = 'INSERT INTO rooms VALUES (0, :title, :size, :price, 
        :bedding, :description, :type_id, :adress_id , :user_id)';
		$pdo = $this->bdd->getPdo();
		$stmt = $pdo->prepare($sql);
		$stmt->execute([
			'title' => $this->title,
			'size' => $this->size,
			'price' => $this->price,
			'bedding' => $this->bedding,
			'description' => $this->description,
			'type_id' => $this->type_id,
			'adress_id' => $this->adress_id,
			'user_id' => $this->user_id
		]);

		if ($stmt->rowCount() == 0) return false;

		$this->id = $pdo->lastInsertId();
		return true;

	}

}
