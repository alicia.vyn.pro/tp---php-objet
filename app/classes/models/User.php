<?php

class User extends Model {

    private $username;
    private $password;
    private $role_id;

    #region Getters & Setters
    public function getUsername()
    {
        return $this->username;
    }
    
    public function setUsername($username): User {
        $this->username = $username;
        return $this;
    }

    public function setPassword($password): User {
        $this->password = $password;
        return $this;
    }

    public function setRole_id($role_id): User {
        $this->role_id = $role_id;
        return $this;
    }
    #endregion


    #region Define abstracts
    protected function getTable(): string {
        return 'users';
    }
    #endregion

    // insertion en BDD
    public function create() {
        $sql = 'INSERT INTO users VALUES (0, :username, :password, :role_id)';
        $pdo = $this->bdd->getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            'username' => $this->username,
            'password' => $this->password,
            'role_id' => $this->role_id
        ]);

        if( $stmt->rowCount() == 0 ) return false;

        $this->id = $pdo->lastInsertId();
        return true;
    }

    // recherche le role_id du user via l'id dans la session 
    public function hasRole( string $role ): bool {

        $sql = 'SELECT role_id FROM users WHERE id=:id';
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute(['id' => $this->id]);
        $result = $stmt->fetch();

        return $role == $result['role_id'];
        
    }


}