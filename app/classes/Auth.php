<?php

class Auth {

    private $username;
    private $password;
    private $password_check;
    private $role_id;

    public function __construct( $username, $password, $password_check = '', $role_id = 0 ) {

        $this->username = $username;
        $this->password = hash('sha256', $password);
        $this->password_check = hash('sha256', $password_check);
        $this->role_id = $role_id;

    }

    // recherche l'utilisateur en BDD correspondant au username et pw donné lors de la connexion
    public function login(): ?User {

        $sql = 'SELECT * FROM users WHERE username=:username AND password=:password';
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute([
            'username' => $this->username,
            'password' => $this->password
        ]);

        $result = $stmt->fetch();

        if( !$result ) return null;

        $user = new User( $result );
        $_SESSION['user'] = $user;

        return $user;

    }

    // vérification que les mots de passes correspondent
    public function checkPasswords(): bool {

        return $this->password == $this->password_check;

    }

    // vérification que l'utilisateur existe bien
    public function userExist(): bool {

        $sql = 'SELECT id FROM users WHERE username = :username';
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute( ['username' => $this->username] );

        return $stmt->rowCount() > 0;

    }

    // enregirstre les informations données dans le formulaire de signin et lance la création dans User.php
    public function signin(): ?User {

        $user = new User;
        $user
            ->setUsername( $this->username )
            ->setPassword( $this->password )
            ->setRole_Id( $this->role_id );

        if( $user->create() ) {
            $_SESSION['user'] = $user;
            return $user;
        } 
        else return null;

    }

    #region Methods static
    // vérifie si un user est enregistré en session
    public static function isLogged(): bool {
        return isset( $_SESSION['user'] );
    }

    // renvoi le user enregistré en session
    public static function user(): ?User {
        return self::isLogged() ? $_SESSION['user'] : null;
    }
    #endregion


}
