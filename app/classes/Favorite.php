<?php

class Favorite {

	private $user;
	private $rooms = [];

	function __construct( User $user ) {
		$this->user = $user;
		$this->getFavorites();
	}

	// recherche tout les favoris si le tableau $rooms = [] est vide
	public function getFavorites(): array {
		if( empty($this->rooms) ) {
			$sql = 'SELECT rooms.* FROM rooms
				JOIN favorites AS f ON rooms.id = f.room_id
				WHERE f.user_id = :user_id';
			$stmt = (new Bdd)->getPdo()->prepare($sql);
			$stmt->execute(['user_id' => $this->user->getId()]);
			$results = $stmt->fetchAll();

			foreach ($results as $result) {
				$room = new Room($result);
				$this->rooms[] = $room;
			}
		}
	return $this->rooms;
	}

	// vérifie si la chambre est déjà en favoris
	function hasRoom( $id ): bool {
		$sql = 'SELECT * FROM favorites
                WHERE user_id = :user_id AND room_id = :room_id';
		$stmt = (new Bdd)->getPdo()->prepare( $sql );

		$stmt->execute([
			'user_id' => $this->user->getId(),
			'room_id' => $id
		]);

		return $stmt->rowCount() > 0;
	}

	// enregristre la chambre dans les favoris dans la table de liaison
	function saveRoom( $id ) {
		$sql = 'INSERT INTO favorites VALUES (:user_id, :room_id)';
		$stmt = (new Bdd)->getPdo()->prepare( $sql );
		$stmt->execute([
			'user_id' => $this->user->getId(),
			'room_id' => $id
		]);

		return $stmt->rowCount() > 0;
	}

	// supprime la chambre des favoris
	function deleteRoom( $id ) {
		$sql = 'DELETE FROM favorites 
                WHERE user_id = :user_id AND room_id = :room_id';
		$stmt = (new Bdd)->getPdo()->prepare( $sql );
		$stmt->execute([
			'user_id' => $this->user->getId(),
			'room_id' => $id
		]);

		return $stmt->rowCount() > 0;

	}


}