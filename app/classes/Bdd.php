<?php 
class Bdd {

    private $host;
    private $dbname;
    private $user;
    private $password;

    public function __construct(
        $host     = DB_HOST,
        $dbname   = DB_NAME,
        $user     = DB_USER,
        $password = DB_PASSWORD
    ) {

        $this->host     = $host;
        $this->dbname   = $dbname;
        $this->user     = $user;
        $this->password = $password;

    }

    public function getPdo(): PDO {

        $dsn = "mysql:dbname={$this->dbname};host={$this->host}";
        $pdo = new PDO( $dsn, $this->user, $this->password );

        // Evite d'écrire PDO::FETCH_ASSOC à chaque requête...
        $pdo->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );

        return $pdo;

    }

}